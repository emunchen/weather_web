import React, { useEffect, useReducer } from "react";
import citiesReducer from "../reducers/cities";
import CityList from "./CityList";
import AppContext from "../context/app-context";
import axios from "axios";
import Spinner from "../assets/spinners/Spinner";

const url = process.env.REACT_APP_URL;
const initialState = { cities: [], isFetching: false, errorMessage: "" };

const App = () => {
  const [{ cities, isFetching, errorMessage }, dispatch] = useReducer(
    citiesReducer,
    initialState
  );

  useEffect(() => {
    dispatch({ type: "GET_CITIES_REQUEST" });
    const GetData = async () => {
      const result = await axios(url + "/cities");
      if (result.data) {
        const data = result.data.cities;
        dispatch({ type: "GET_CITIES_SUCCESS", payload: data });
      }
    };
    GetData();
  }, []);

  return (
    <AppContext.Provider value={cities}>
      <div className="container mx-auto px-6 text-blue-800 bg-gray-100">
        <h1 className="font-semibold text-5xl sm:text-center md:text-center lg:text-left xl:text-left">
          Weather App
        </h1>
        {isFetching && (
          <h2>
            Loading <Spinner />
          </h2>
        )}
        {errorMessage && <h2>{errorMessage}</h2>}
        <CityList />
      </div>
    </AppContext.Provider>
  );
};

export { App as default };
