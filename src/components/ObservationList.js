import React, { useContext } from "react";
import Observation from "./Observation";
import AppContext from "../context/app-context";

const ObservationList = () => {
  const observations = useContext(AppContext);
  return (
    <ul className="border border-gray-400 bg-white mt-4">
      {observations.map((observation) => (
        <Observation key={observation._id} observation={observation} />
      ))}
    </ul>
  );
};

export { ObservationList as default };
