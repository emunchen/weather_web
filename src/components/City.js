import React, { useContext } from "react";
import AppContext from "../context/app-context";
import faker from "faker";
import AddObservationForm from "./AddObservationForm";
import ObservationContainer from "./ObservationContainer";

const City = ({ city }) => {
  const { dispatch } = useContext(AppContext);

  return (
    <div>
      <div className="max-w-sm w-full lg:max-w-full lg:flex py-6">
        <div
          className="h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden"
          title="Woman holding a mug"
          style={{
            backgroundImage: `url(https://picsum.photos/seed/${Math.random()
              .toString(36)
              .substring(7)}/192/461)`,
          }}
        ></div>
        <div className="sm:w-auto md:w-auto lg:w-full  xl:w-full border-r border-b border-l border-gray-400 lg:border-l-0 lg:border-t lg:border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
          <div className="mb-8">
            <p className="text-sm text-gray-600 flex items-center">
              <svg
                className="fill-current text-gray-500 w-3 h-3 mr-2"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
              >
                <path d="M4 8V6a6 6 0 1 1 12 0v2h1a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2v-8c0-1.1.9-2 2-2h1zm5 6.73V17h2v-2.27a2 2 0 1 0-2 0zM7 6v2h6V6a3 3 0 0 0-6 0z" />
              </svg>
              Members only
            </p>
            <div className="text-gray-900 font-bold text-xl mb-2">
              {city.name}
            </div>
            <p className="text-gray-700 text-base">Wind: {city.wind} kps</p>
            <p className="text-gray-700 text-base">
              Latitude: {city.latitude.value}{" "}
              {city.latitude.orientation.toUpperCase()}
            </p>
            <p className="text-gray-700 text-base">
              Longitude: {city.longitude.value}{" "}
              {city.longitude.orientation.toUpperCase()}
            </p>
            <button
              onClick={() => dispatch({ type: "REMOVE_CITY", _id: city._id })}
            >
              Remove
            </button>
          </div>
          <div className="flex items-center">
            <img
              className="w-10 h-10 rounded-full mr-4"
              src={`https://picsum.photos/seed/${Math.random()
                .toString(36)
                .substring(7)}/100/100`}
              alt="Avatar of Jonathan Reinink"
            />
            <div className="text-sm">
              <p className="text-gray-900 leading-none">{`${faker.name.firstName()} ${faker.name.lastName()}`}</p>
              <p className="text-gray-600">
                Date: {new Date().toLocaleDateString()}
              </p>
            </div>
          </div>
          <AddObservationForm city={city} />
        </div>
      </div>
      <ObservationContainer city={city} />
    </div>
  );
};

export { City as default };
