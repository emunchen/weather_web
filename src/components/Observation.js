import React from "react";

const Observation = ({ observation }) => {
  return (
    <li className="w-full bg-white p-4 text-gray-700 text-lg">
      <div>{observation.text} </div>
      <div className="text-gray-800 text-xs">
        created at: {Date(observation.created_at)} by:{" "}
        <a href={`mailto:${observation.user.email}`}>
          {observation.user.email}
        </a>
      </div>
    </li>
  );
};

export { Observation as default };
