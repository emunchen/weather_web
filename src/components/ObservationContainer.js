import React, { useReducer } from "react";
import AppContext from "../context/app-context";
import ObservationList from "./ObservationList";
import observationReducer from "../reducers/observations";
import Spinner from "../assets/spinners/Spinner"
import axios from "axios";

const url = process.env.REACT_APP_URL;
const initialState = { observations: [], isFetching: false, errorMessage: "" };

const ObservationContainer = ({ city }) => {
  const [{ observations, isFetching }, dispatch] = useReducer(
    observationReducer,
    initialState
  );

  const fetchObservations = async () => {
    dispatch({ type: "GET_OBSERVATION_REQUEST" });
    const result = await axios(`${url}/observations/${city._id}/cities`);
    if (result.data) {
      const data = result.data.observations;
      dispatch({ type: "GET_OBSERVATION_SUCCESS", payload: data });
    }
  };

  return (
    <AppContext.Provider value={observations}>
      <div>
        <button
          onClick={fetchObservations}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          type="button"
        >
          Show Comments {isFetching && <Spinner />}
        </button>
        <ObservationList />
      </div>
    </AppContext.Provider>
  );
};

export { ObservationContainer as default };
