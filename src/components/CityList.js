import React, { useContext } from "react";
import City from "./City";
import AppContext from "../context/app-context";

const CityList = () => {
  const cities = useContext(AppContext);
  return cities.map(city => <City key={city._id} city={city} />);
};

export { CityList as default };
