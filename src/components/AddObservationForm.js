import React, { useState } from "react";
import Spinner from "../assets/spinners/Spinner";
import axios from "axios";

const url = process.env.REACT_APP_URL;

const AddObservationForm = ({ city }) => {
  const [email, setEmail] = useState("");
  const [text, setText] = useState("");
  const [loading, setLoading] = useState(false);
  const addObservation = (e) => {
    e.preventDefault();
    setLoading(true);
    const PostObservation = async () => {
      if (!email || !text || !city) {
        setLoading(false);
        return;
      }

      const result = await axios.post(url + "/observations", {
        email: email,
        text: text,
        city_id: city._id,
      });
      if (result.data) {
        setLoading(false);
      }
    };
    PostObservation();
    setEmail("");
    setText("");
  };

  return (
    <div>
      <form onSubmit={addObservation} className="mt-4">
        <input
          value={email}
          placeholder="Your Email"
          onChange={(e) => setEmail(e.target.value)}
          className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 mb-4"
        />
        <textarea
          value={text}
          placeholder="Add some text ..."
          onChange={(e) => setText(e.target.value)}
          className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500 mb-4"
        ></textarea>
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          type="submit"
        >
          Add {loading && <Spinner />}
        </button>
      </form>
    </div>
  );
};

export { AddObservationForm as default };
