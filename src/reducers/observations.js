export const initialState = { observations: [], isFetching: false, errorMessage: "" };

const observationReducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_OBSERVATION_REQUEST":
      return { ...state, isFetching: true, observations: [] };
    case "GET_OBSERVATION_SUCCESS":
      return { ...state, isFetching: false, observations: action.payload };
    case "GET_OBSERVATION_FAILURE":
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload.message,
      };
    default:
      return state;
  }
};

export { observationReducer as default };
