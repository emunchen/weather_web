export const initialState = { cities: [], isFetching: false, errorMessage: "" };

const citiesReducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_CITIES_REQUEST":
      return { ...state, isFetching: true, cities: [] };
    case "GET_CITIES_SUCCESS":
      return { ...state, isFetching: false, cities: action.payload };
    case "GET_CITIES_FAILURE":
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload.message
      };
    default:
      return state;
  }
};

export { citiesReducer as default };
