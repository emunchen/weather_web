import React from "react";
import { shallow } from "enzyme";
import AddObservationForm from "../../components/AddObservationForm";

test("should render with email value", () => {
  const wrapper = shallow(<AddObservationForm />);
  wrapper.find('input[placeholder="Your Email"]').simulate("change", {
    target: {
      value: "test@test.com",
    },
  });
  expect(wrapper.find('input[placeholder="Your Email"]').prop("value")).toEqual(
    "test@test.com"
  );
});

test("should render with a textarea value", () => {
  const wrapper = shallow(<AddObservationForm />);
  wrapper.find("textarea").simulate("change", {
    target: {
      value: "This is a text note",
    },
  });
  expect(wrapper.find("textarea").prop("value")).toEqual("This is a text note");
});

test("should call onSubmit prop for valid submission.", () => {
  const onSubmitSpy = jest.fn();
  onSubmitSpy();
  expect(onSubmitSpy).toHaveBeenCalled();
});
