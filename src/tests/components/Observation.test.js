import React from "react";
import { shallow } from "enzyme";
import Observation from "../../components/Observation";
import observations from "../fixtures/observations";

test("should render Observation correctly", () => {
  const wrapper = shallow(<Observation observation={observations[0]} />);
  expect(wrapper).toMatchSnapshot();
});
