import citiesReducer from "../../reducers/cities";
import { initialState } from "../../reducers/cities";
import cities from "../fixtures/cities";

test("should set default state", () => {
  const state = citiesReducer(undefined, { type: "@@INIT" });
  expect(state).toEqual(initialState);
});

test("should set state with fetching in true", () => {
  const state = citiesReducer(undefined, { type: "GET_CITIES_REQUEST" });
  const newState = { errorMessage: "", isFetching: true, cities: [] };
  expect(state).toEqual(newState);
});

test("should set state with cities payload", () => {
  const state = citiesReducer(undefined, {
    type: "GET_CITIES_SUCCESS",
    payload: cities,
  });
  const newState = { errorMessage: "", isFetching: false, cities: cities };
  expect(state).toEqual(newState);
});

test("should set state with errorMessage", () => {
  const state = citiesReducer(undefined, {
    type: "GET_CITIES_FAILURE",
    payload: { message: "Error fetching cities" },
  });
  const newState = {
    errorMessage: "Error fetching cities",
    isFetching: false,
    cities: [],
  };
  expect(state).toEqual(newState);
});
