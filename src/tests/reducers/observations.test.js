import observationReducer from "../../reducers/observations";
import { initialState } from "../../reducers/observations";
import observations from "../fixtures/observations";

test("should set default state", () => {
  const state = observationReducer(undefined, { type: "@@INIT" });
  expect(state).toEqual(initialState);
});

test("should set state with fetching in true", () => {
  const state = observationReducer(undefined, {
    type: "GET_OBSERVATION_REQUEST",
  });
  const newState = { errorMessage: "", isFetching: true, observations: [] };
  expect(state).toEqual(newState);
});

test("should set state with observations payload", () => {
  const state = observationReducer(undefined, {
    type: "GET_OBSERVATION_SUCCESS",
    payload: observations,
  });
  const newState = {
    errorMessage: "",
    isFetching: false,
    observations: observations,
  };
  expect(state).toEqual(newState);
});

test("should set state with errorMessage", () => {
  const state = observationReducer(undefined, {
    type: "GET_OBSERVATION_FAILURE",
    payload: { message: "Error fetching observations" },
  });
  const newState = {
    errorMessage: "Error fetching observations",
    isFetching: false,
    observations: [],
  };
  expect(state).toEqual(newState);
});
