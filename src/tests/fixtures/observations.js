export default [
  {
    _id: "5e87cfe99ade566eff7ecd84",
    text:
      "Sed in volutpat lorem, quis sodales nunc. Nulla in lobortis neque. Donec turpis tellus, viverra non sollicitudin iaculis, aliquet eget orci. Mauris malesuada luctus elementum",
    user: {
      _id: "5e87cfe99ade566eff7ecd83",
      email: "emanue",
      created_at: "2020-04-04T00:08:09.588Z",
    },
    city: {
      latitude: {
        orientation: "n",
        value: "51.5074",
      },
      longitude: {
        orientation: "w",
        value: "0.1278",
      },
      _id: "5e87cf37f44da8787872852c",
      slug: "london_united_kingdom",
      wind: 34,
      name: "London, United Kingdom",
    },
    created_at: "2020-04-04T00:08:09.590Z",
  },
  {
    _id: "5e87d0009ade566eff7ecd86",
    text:
      "Donec rutrum mauris a purus rhoncus aliquet. Maecenas vel dolor vitae purus semper sodales.",
    user: {
      _id: "5e87d0009ade566eff7ecd85",
      email: "lorem@rutrum.com",
      created_at: "2020-04-04T00:08:32.442Z",
    },
    city: {
      latitude: {
        orientation: "n",
        value: "51.5074",
      },
      longitude: {
        orientation: "w",
        value: "0.1278",
      },
      _id: "5e87cf37f44da8787872852c",
      slug: "london_united_kingdom",
      wind: 34,
      name: "London, United Kingdom",
    },
    created_at: "2020-04-04T00:08:32.443Z",
  },
  {
    _id: "5e87d01a9ade566eff7ecd88",
    text:
      "Aenean eu lacus et nunc porta bibendum. Suspendisse libero dolor, malesuada quis nisi at, fringilla ultricies diam. Donec iaculis quam a neque condimentum",
    user: {
      _id: "5e87d01a9ade566eff7ecd87",
      email: "city@uk.com",
      created_at: "2020-04-04T00:08:58.140Z",
    },
    city: {
      latitude: {
        orientation: "n",
        value: "51.5074",
      },
      longitude: {
        orientation: "w",
        value: "0.1278",
      },
      _id: "5e87cf37f44da8787872852c",
      slug: "london_united_kingdom",
      wind: 34,
      name: "London, United Kingdom",
    },
    created_at: "2020-04-04T00:08:58.141Z",
  },
];
