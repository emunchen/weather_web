export default [
  {
    latitude: {
      orientation: "n",
      value: "51.5074",
    },
    longitude: {
      orientation: "w",
      value: "0.1278",
    },
    _id: "5e87cf37f44da8787872852c",
    slug: "london_united_kingdom",
    wind: 2,
    name: "London, United Kingdom",
  },
  {
    latitude: {
      orientation: "n",
      value: "41.7151",
    },
    longitude: {
      orientation: "e",
      value: "44.827",
    },
    _id: "5e87cf37f44da8787872852d",
    slug: "tbilisi_georgia",
    wind: 31,
    name: "Tbilisi, Georgia",
  },
  {
    latitude: {
      orientation: "n",
      value: "19.4326",
    },
    longitude: {
      orientation: "w",
      value: "99.1332",
    },
    _id: "5e87cf37f44da8787872852e",
    slug: "mexico_city_mexico",
    wind: 13,
    name: "Mexico City, Mexico",
  },
];
